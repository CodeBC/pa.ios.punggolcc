//
//  TTFloorPlanCell.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTFloorPlanObject : NSObject

@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *title;

+ (id)floorPlanWithTitle:(NSString *)title imageURL:(NSString *)imageURL;

@end

@interface TTFloorPlanCell : UITableViewCell <NICell>

@end
