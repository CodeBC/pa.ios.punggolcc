//
//  TTHTMLTemplateViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/28/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTHTMLTemplateViewController.h"

@interface TTHTMLTemplateViewController ()

@property (nonatomic, strong) UIBarButtonItem *loadingIndicatorItem;

@end

@implementation TTHTMLTemplateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    _webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    _webView.delegate = self;
    _webView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
    _webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:_webView];
    
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] init];
    [activityIndicatorView startAnimating];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:activityIndicatorView];
    _loadingIndicatorItem = item;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadHTML];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    self.title = NSLocalizedString(@"Loading...", nil);
    self.navigationItem.rightBarButtonItem = self.loadingIndicatorItem;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.navigationItem.rightBarButtonItem = nil;
    self.title = self.viewTitle;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    self.title = NSLocalizedString(@"Error", nil);
    self.navigationItem.rightBarButtonItem = nil;
}

- (NSString *)viewTitle
{
    return @"";
}

- (NSString *)HTMLFileName
{
    return @"event";
}

- (NSString *)HTMLTemplate
{
    NSURL *url = [[NSBundle mainBundle] URLForResource:[self HTMLFileName] withExtension:@"html"];
    return [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
}

- (NSDictionary *)replaceValues
{
    return @{};
}

- (void)loadHTML
{
    if (self.webView == nil) {
        return;
    }
    NSMutableString *HTML = [[self HTMLTemplate] mutableCopy];
    NSDictionary *variables = [self replaceValues];
    
    for (NSString *key in variables.allKeys) {
        NSString *placeholder = [NSString stringWithFormat:@"<!--%@-->", key];
        [HTML replaceOccurrencesOfString:placeholder
                              withString:variables[key]
                                 options:NSLiteralSearch
                                   range:NSMakeRange(0, HTML.length)];
    }
    [_webView loadHTMLString:HTML baseURL:[[NSBundle mainBundle] resourceURL]];
}

@end
