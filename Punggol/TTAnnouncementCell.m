//
//  TTSocialMediaCell.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/16/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "TTAnnouncementCell.h"
#import "Feed.h"

#define CELL_PADDING 5.0f
#define CONTENT_PADDING 10.0f
#define VIEW_PADDING 3.0f
#define TITLE_FONT [UIFont boldSystemFontOfSize:14.0f]
#define TIME_FONT [UIFont boldSystemFontOfSize:13.0f]
#define DESC_FONT [UIFont systemFontOfSize:13.0f]
#define BUTTON_HEIGHT 36.0f
#define BUTTON_TOP_PADDING 20.0f

#define CELL_EDGE_INSETS UIEdgeInsetsMake(CELL_PADDING, CELL_PADDING, CELL_PADDING, CELL_PADDING)
#define CONTENT_EDGE_INSETS UIEdgeInsetsMake(CONTENT_PADDING, CONTENT_PADDING, CONTENT_PADDING, CONTENT_PADDING)

@implementation TTAnnouncementCell

+ (CGFloat)heightForObject:(Feed *)feed atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    CGFloat height = 0;
    
    CGFloat contentWidth = tableView.width - (CELL_PADDING + CONTENT_PADDING) * 2;
    CGSize contentSize = CGSizeMake(contentWidth, CGFLOAT_MAX);
    
    height += [feed.title sizeWithFont:TITLE_FONT
                      constrainedToSize:contentSize
                          lineBreakMode:NSLineBreakByWordWrapping].height + VIEW_PADDING;
    
    height += [feed.desc sizeWithFont:DESC_FONT
                           constrainedToSize:contentSize
                               lineBreakMode:NSLineBreakByWordWrapping].height + VIEW_PADDING;
        
    height += (CELL_PADDING + CONTENT_PADDING)*2;
    
    return height;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.numberOfLines = 2;
        self.titleLabel.font = TITLE_FONT;
        [self.contentView addSubview:self.titleLabel];
        
        self.descLabel = [[RTLabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 300.0f, 300.0f)];
//        self.descLabel.numberOfLines = 0;
        self.descLabel.font = DESC_FONT;
        self.descLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:self.descLabel];
        
        self.contentView.layer.borderWidth = 1.0f;
        self.contentView.layer.borderColor = [UIColor colorWithWhite:0.75f alpha:1.0f].CGColor;
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    Feed *feed = (Feed *)object;
    if ([feed isKindOfClass:[Feed class]]) {
        self.titleLabel.text = feed.title;
        self.descLabel.text = [feed.desc stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    }
    return YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.contentView.frame = UIEdgeInsetsInsetRect(self.contentView.bounds, CELL_EDGE_INSETS);
    
    CGRect frame = UIEdgeInsetsInsetRect(self.contentView.bounds, CONTENT_EDGE_INSETS);
    
    CGSize sizeToFit;
    CGRect viewFrame;
    
    sizeToFit = [self.titleLabel sizeThatFits:CGSizeMake(frame.size.width, CGFLOAT_MAX)];
    viewFrame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, sizeToFit.height);
    self.titleLabel.frame = viewFrame;
    
    sizeToFit = [self.descLabel sizeThatFits:CGSizeMake(frame.size.width, CGFLOAT_MAX)];
    viewFrame = CGRectMake(frame.origin.x, CGRectGetMaxY(viewFrame) + VIEW_PADDING, frame.size.width, sizeToFit.height + 20);
    self.descLabel.frame = viewFrame;
}

@end