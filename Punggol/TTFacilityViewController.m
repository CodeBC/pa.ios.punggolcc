//
//  TTFacilityViewController.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTFacilityViewController.h"
#import "TTMultiImagesView.h"
#import "TTImageViewController.h"
#import "UIApplication+PhoneCall.h"

@interface TTFacilityViewController () <MFMailComposeViewControllerDelegate, TTMultiImagesViewDelegate>

@property (nonatomic, strong) TTMultiImagesView *imagesView;

@end

@implementation TTFacilityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    self.imagesView = [[TTMultiImagesView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 150.0)];
    self.imagesView.delegate = self;
    [self.view addSubview:self.imagesView];
    
    self.webView.frame = CGRectMake(0, self.imagesView.height, self.view.width, self.webView.height - self.imagesView.height);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if([[self.facility.facilityImages allObjects] count] == 0) {
        self.imagesView.images = [NSArray arrayWithObjects:@"https://s3.amazonaws.com/punggol/Facilities-5.JPG",nil];
    } else {
        self.imagesView.images = [[self.facility.facilityImages allObjects] arrayOfKeyPath:@"image"];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([request.URL.scheme isEqualToString:@"punggol"] && [request.URL.host isEqualToString:@"facility"]) {
        if ([request.URL.path isEqualToString:@"/callPhone"]) {
            [[UIApplication sharedApplication] callPhone:self.facility.contactNumber];
            return NO;
        } else if ([request.URL.path isEqualToString:@"/sendEmail"]) {
            MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
            [mailVC setToRecipients:@[self.facility.email]];
            mailVC.mailComposeDelegate = self;
            [self presentViewController:mailVC animated:YES completion:NULL];
            return NO;
        }
    }
    return YES;
}

- (NSString *)viewTitle
{
    return @"Facility";
}

- (NSString *)HTMLFileName
{
    return @"facility";
}

- (NSDictionary *)replaceValues
{
    if (self.facility == nil) {
        return @{};
    }
    
    return @{
             @"title"   : self.facility.title,
             @"desc"    : self.facility.desc
    };
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - TTMultiImagesViewDelegate

- (void)multiImagesView:(TTMultiImagesView *)imagesView didTapImageView:(TTImageView *)imageView
{
    TTImageViewController *imageVC = [[TTImageViewController alloc] initWithImage:imageView.image];
    imageVC.title = NSLocalizedString(@"Facility", nil);
    [self.navigationController pushViewController:imageVC animated:YES];
}

@end
