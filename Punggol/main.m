//
//  main.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/9/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TTAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TTAppDelegate class]));
    }
}
