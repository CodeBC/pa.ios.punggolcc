//
//  FacilityImage.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "FacilityImage.h"


@implementation FacilityImage

@dynamic image;
@dynamic cropImage;
@dynamic facility;

@end
