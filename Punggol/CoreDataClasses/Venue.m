//
//  Venue.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/11/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Venue.h"


@implementation Venue

@dynamic id;
@dynamic name;
@dynamic location;
@dynamic longitude;
@dynamic latitude;
@dynamic events;

@end
