//
//  Facility+Additions.h
//  Punggol
//
//  Created by Ye Myat Min on 20/7/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Facility.h"

@interface NSArray (FacilityAddition)

- (NSArray *)groupFacilityByName;
- (NSArray *)groupFacilityByLevel;

@end
