//
//  Facility+Additions.m
//  Punggol
//
//  Created by Ye Myat Min on 20/7/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.

#import "Facility+Additions.h"
#import "NSArray+TTAddition.h"


@implementation NSArray (FacilityAddition)

- (NSArray *)groupFacilityByName
{
    return [self groupedByKeyPath:@"title"];
}

- (NSArray *)groupFacilityByLevel
{
    return [self groupedByKeyPath:@"level" defaultGroup:@" " onlyFirstCharacter:NO];
}

@end

