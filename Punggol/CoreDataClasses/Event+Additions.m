//
//  Event+Additions.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/23/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Event+Additions.h"

@implementation Event (Additions)

- (NSString *)eventDuration;
{
    static NSDateFormatter *formatter;
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd MMM yyyy (HH:mm)";
        formatter.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    }
    return [NSString stringWithFormat:@"%@ - %@",
            [formatter stringFromDate:self.startDateTime],
            [formatter stringFromDate:self.endDateTime]];
}

@end
