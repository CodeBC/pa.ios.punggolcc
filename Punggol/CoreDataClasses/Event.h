//
//  Event.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Contact, Guest, Organizer, Venue;

@interface Event : NSManagedObject

@property (nonatomic, retain) NSString * cropImage;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSDate * endDateTime;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSDate * startDateTime;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *contacts;
@property (nonatomic, retain) NSSet *guestOfHonours;
@property (nonatomic, retain) NSSet *organizers;
@property (nonatomic, retain) Venue *venue;
@end

@interface Event (CoreDataGeneratedAccessors)

- (void)addContactsObject:(Contact *)value;
- (void)removeContactsObject:(Contact *)value;
- (void)addContacts:(NSSet *)values;
- (void)removeContacts:(NSSet *)values;

- (void)addGuestOfHonoursObject:(Guest *)value;
- (void)removeGuestOfHonoursObject:(Guest *)value;
- (void)addGuestOfHonours:(NSSet *)values;
- (void)removeGuestOfHonours:(NSSet *)values;

- (void)addOrganizersObject:(Organizer *)value;
- (void)removeOrganizersObject:(Organizer *)value;
- (void)addOrganizers:(NSSet *)values;
- (void)removeOrganizers:(NSSet *)values;

@end
