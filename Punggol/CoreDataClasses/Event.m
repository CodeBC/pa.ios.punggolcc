//
//  Event.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Event.h"
#import "Contact.h"
#import "Guest.h"
#import "Organizer.h"
#import "Venue.h"


@implementation Event

@dynamic cropImage;
@dynamic desc;
@dynamic email;
@dynamic endDateTime;
@dynamic id;
@dynamic image;
@dynamic phone;
@dynamic startDateTime;
@dynamic title;
@dynamic contacts;
@dynamic guestOfHonours;
@dynamic organizers;
@dynamic venue;

@end
