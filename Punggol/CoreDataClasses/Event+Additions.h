//
//  Event+Additions.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/23/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Event.h"

@interface Event (Additions)

- (NSString *)eventDuration;

@end
