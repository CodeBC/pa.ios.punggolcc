//
//  Announcement.m
//  Punggol
//
//  Created by Ye Myat Min on 17/7/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Feed.h"

@implementation Feed : NSManagedObject

@dynamic id;
@dynamic title;
@dynamic desc;
@dynamic deliveredDate;

@end
