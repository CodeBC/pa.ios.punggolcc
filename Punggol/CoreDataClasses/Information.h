//
//  Information.h
//  Punggol
//
//  Created by Ye Myat Min on 22/7/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Information : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * coursesLink;
@property (nonatomic, retain) NSNumber * id;


@end
