//
//  Announcement.h
//  Punggol
//
//  Created by Ye Myat Min on 17/7/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Feed : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSDate * deliveredDate;
@property (nonatomic, retain) NSNumber * id;


@end
