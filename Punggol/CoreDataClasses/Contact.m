//
//  Contact.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/11/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Contact.h"


@implementation Contact

@dynamic address;
@dynamic email;
@dynamic name;
@dynamic phone;
@dynamic title;
@dynamic desc;
@dynamic event;

@end
