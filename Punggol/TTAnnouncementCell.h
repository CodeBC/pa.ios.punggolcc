//
//  TTAnnouncementCell.h
//  Punggol
//
//  Created by Ye Myat Min on 17/7/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTLabel.h"

@interface TTAnnouncementCell : UITableViewCell <NICell>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) RTLabel *descLabel;
@property (nonatomic, strong) UILabel *dateLabel;

@end
