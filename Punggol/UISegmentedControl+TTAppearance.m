//
//  UISegmentedControl+TTAppearance.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "UISegmentedControl+TTAppearance.h"

@implementation UISegmentedControl (TTAppearance)

- (void)applyWhiteStyle
{
    UISegmentedControl *segmentControl = self;
    NSDictionary *textAttributes = @{
                                     UITextAttributeTextColor: [UIColor blackColor],
                                     UITextAttributeTextShadowColor : [UIColor whiteColor]
                                     };
    [segmentControl setTitleTextAttributes:textAttributes
                                  forState:UIControlStateNormal];
    [segmentControl setBackgroundImage:[UIImage imageNamed:@"SegmentButton"]
                              forState:UIControlStateNormal
                            barMetrics:UIBarMetricsDefault];
    [segmentControl setBackgroundImage:[UIImage imageNamed:@"SegmentButtonSelected"]
                              forState:UIControlStateSelected
                            barMetrics:UIBarMetricsDefault];
    [segmentControl setDividerImage:[UIImage imageNamed:@"SegmentDividerLeftSelected"]
                forLeftSegmentState:UIControlStateSelected
                  rightSegmentState:UIControlStateNormal
                         barMetrics:UIBarMetricsDefault];
    [segmentControl setDividerImage:[UIImage imageNamed:@"SegmentDividerRightSelected"]
                forLeftSegmentState:UIControlStateNormal
                  rightSegmentState:UIControlStateSelected
                         barMetrics:UIBarMetricsDefault];
    [segmentControl setDividerImage:[UIImage imageNamed:@"SegmentDividerUnSelected"]
                forLeftSegmentState:UIControlStateNormal
                  rightSegmentState:UIControlStateNormal
                         barMetrics:UIBarMetricsDefault];
}

@end
