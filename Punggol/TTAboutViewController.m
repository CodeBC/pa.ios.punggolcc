//
//  TTAboutViewController.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/10/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTAboutViewController.h"
#import "MGMushParser.h"
#import "TTMapViewController.h"
#import "BButton.h"
#import "UIApplication+PhoneCall.h"

// disable selection on textview - quick and dirty
// http://stackoverflow.com/a/8013538/1275944

@implementation UITextView (DisableCopyPaste)

- (BOOL)canBecomeFirstResponder
{
    return NO;
}

@end

@interface TTAboutViewController () <UIScrollViewDelegate, MFMailComposeViewControllerDelegate>

@end

@implementation TTAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"About", nil);
        self.tabBarItem.image = [UIImage imageNamed:@"166-newspaper"];

    }
    return self;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)loadView
{
    [super loadView];
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height)];
    self.textView.contentInset = UIEdgeInsetsMake(5, 5, 5, 5);
    self.textView.editable = NO;
    self.textView.dataDetectorTypes = UIDataDetectorTypePhoneNumber | UIDataDetectorTypeLink;

    UIFont *baseFont = [UIFont fontWithName:@"HelveticaNeue" size:14.0f];
    UIColor *textColor = [UIColor blackColor];
    
    NSString *markdown =
    @"Punggol CC is located near Hougang Central and a few minutes walk away from the Hougang MRT station and bus interchange. There are a number of creative, fun and sports activities at the CC that you can join for relaxation or recreation.\n\n"
    @"Bus Service\n"
    @"27,62,74,80,82,84,88,89,97,111,136,147,153,165,511,502, Feeder Service 325 (Hougang Central Interchange) Feeder Service 326 (Hougang South Interchange)\n\n"
    @"Operating Hours: **09:00 - 22:00**\n"
    @"Payment Hours: **09:30 - 21:30**\n";
    
    self.textView.height = 400.0f;
    self.textView.scrollEnabled = NO;
    
    NSAttributedString *attributedString = [MGMushParser attributedStringFromMush:markdown
                                                                             font:baseFont
                                                                            color:textColor];
    
    self.textView.attributedText = attributedString;
    
    BButton *locateButton = [[BButton alloc] initWithFrame:CGRectMake(10, 300, 100, 40) color:[UIColor colorWithRed:0.90f green:0.00f blue:0.00f alpha:1.00f]];
    [locateButton setTitle:@"Locate Us" forState:UIControlStateNormal];
    [locateButton addTarget:self action:@selector(locateUs) forControlEvents:UIControlEventTouchUpInside];
    
    BButton *callButton = [[BButton alloc] initWithFrame:CGRectMake(120, 300, 80, 40) color:[UIColor colorWithRed:0.90f green:0.00f blue:0.00f alpha:1.00f]];
    [callButton setTitle:@"Call" forState:UIControlStateNormal];
    [callButton addTarget:self action:@selector(callUs) forControlEvents:UIControlEventTouchUpInside];
    
    BButton *emailButton = [[BButton alloc] initWithFrame:CGRectMake(210, 300, 80, 40) color:[UIColor colorWithRed:0.90f green:0.00f blue:0.00f alpha:1.00f]];
    [emailButton setTitle:@"Email" forState:UIControlStateNormal];
    [emailButton addTarget:self action:@selector(emailUs) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 450.0f)];
    [containerView addSubview:self.textView];
    [containerView addSubview:locateButton];
    [containerView addSubview:callButton];
    [containerView addSubview:emailButton];

    
    CGRect backgroundRect = CGRectMake(0, 0, self.view.frame.size.width, 250.0f);
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:backgroundRect];
    [backgroundImageView setImageWithURL:[NSURL URLWithString:@"https://s3.amazonaws.com/punggol/punggolcc-banner.jpg"]
                   placeholderImage:nil];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    MDCParallaxView *parallaxView = [[MDCParallaxView alloc] initWithBackgroundView:backgroundImageView
                                                                     foregroundView:containerView];
    parallaxView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    parallaxView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    parallaxView.backgroundHeight = 100.0f;
    parallaxView.scrollView.scrollsToTop = YES;
    parallaxView.backgroundInteractionEnabled = YES;
    parallaxView.scrollViewDelegate = self;
    [self.view addSubview:parallaxView];
}

- (void)locateUs
{
    TTMapViewController *mapVC = [[TTMapViewController alloc] init];
    mapVC.title = NSLocalizedString(@"Punggol Location", nil);
    [self.navigationController pushViewController:mapVC animated:YES];
}

- (void)callUs
{
    [[UIApplication sharedApplication] callPhone:@"63871833"];
}

- (void)emailUs
{
    MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
    [mailVC setToRecipients:@[@"PA_PUNGGOLCC@pa.gov.sg"]];
    mailVC.mailComposeDelegate = self;
    [self presentViewController:mailVC animated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

@end
