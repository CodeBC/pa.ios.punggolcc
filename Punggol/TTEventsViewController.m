//
//  TTEventsViewController.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/10/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTEventsViewController.h"
#import "Event.h"
#import "Information.h"
#import "UIApplication+PhoneCall.h"
#import "TTEventCell.h"
#import "TTImageViewController.h"
#import "TTEventDetailViewController.h"

@interface TTEventsViewController () <NITableViewModelDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) UISegmentedControl *segmentControl;
@property (nonatomic, strong) NSString * coursesLink;

@end

@implementation TTEventsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Activities", nil);
        [self.cellFactory mapObjectClass:[Event class] toCellClass:[TTEventCell class]];
        self.tabBarItem.image = [UIImage imageNamed:@"myschedule"];
    }
    return self;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.tableModel = [[NITableViewModel alloc] initWithListArray:[self showingEvents] delegate:self];
    [self.tableView reloadData];
}


- (void)loadView
{
    [super loadView];
    
    // Segmented Control
    
    UISegmentedControl *segmentControl = [[UISegmentedControl alloc] initWithItems:@[@"Upcoming Events", @"Past Events"]];
    segmentControl.segmentedControlStyle = UISegmentedControlStyleBar;


    [segmentControl sizeToFit];
    segmentControl.frame = CGRectMake(0, 0, self.view.width, segmentControl.height);
    [segmentControl addTarget:self action:@selector(didSwitchEventMode:) forControlEvents:UIControlEventValueChanged];
    [segmentControl setSelectedSegmentIndex:0];
    [segmentControl setBackgroundColor:[UIColor whiteColor]];
    
    NSDictionary *textAttributes = @{
                                     UITextAttributeTextColor: [UIColor blackColor],
                                     UITextAttributeTextShadowColor : [UIColor whiteColor]
                                     };
    [segmentControl setTitleTextAttributes:textAttributes
                                  forState:UIControlStateNormal];
    [segmentControl setTitleTextAttributes:textAttributes
                                  forState:UIControlStateSelected];
    [segmentControl setBackgroundImage:[UIImage imageNamed:@"SegmentButton"]
                              forState:UIControlStateNormal
                            barMetrics:UIBarMetricsDefault];
    [segmentControl setBackgroundImage:[UIImage imageNamed:@"SegmentButtonSelected"]
                              forState:UIControlStateSelected
                            barMetrics:UIBarMetricsDefault];
    [segmentControl setDividerImage:[UIImage imageNamed:@"SegmentDividerLeftSelected"]
                forLeftSegmentState:UIControlStateSelected
                  rightSegmentState:UIControlStateNormal
                         barMetrics:UIBarMetricsDefault];
    [segmentControl setDividerImage:[UIImage imageNamed:@"SegmentDividerRightSelected"]
                forLeftSegmentState:UIControlStateNormal
                  rightSegmentState:UIControlStateSelected
                         barMetrics:UIBarMetricsDefault];
    [segmentControl setDividerImage:[UIImage imageNamed:@"SegmentDividerUnSelected"]
                forLeftSegmentState:UIControlStateNormal
                  rightSegmentState:UIControlStateNormal
                         barMetrics:UIBarMetricsDefault];
    
    [self.view addSubview:segmentControl];
    _segmentControl = segmentControl;
    
    self.tableView.frame = CGRectMake(0,
                                      self.segmentControl.height,
                                      self.view.width,
                                      self.tableView.height - self.segmentControl.height);
    self.tableView.rowHeight = 100;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"cream_pixels.png"]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self upcomingEvents];
}

- (NSArray *)upcomingEvents
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"startDateTime > %@", [NSDate date]];
    NSArray *events = [Event MR_findAllWithPredicate:predicate];
    NSArray *sortedEvents = [events sortedArrayUsingComparator:^NSComparisonResult(Event *e1, Event *e2) {
        return [e1.startDateTime compare:e2.startDateTime];
    }];
    
    if([sortedEvents count] == 0) {
        [SVProgressHUD showErrorWithStatus:@"No events found"];
    }
    
    return sortedEvents;
}

- (NSArray *)pastEvents
{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"startDateTime < %@",
                              [NSDate date]];
    
    NSArray *events = [Event MR_findAllWithPredicate:predicate];
    NSArray *sortedEvents = [events sortedArrayUsingComparator:^NSComparisonResult(Event *e1, Event *e2) {
        return [e1.startDateTime compare:e2.startDateTime];
    }];
    
    if([sortedEvents count] == 0) {
        [SVProgressHUD showErrorWithStatus:@"No events found"];
    }
    
    return [[sortedEvents reverseObjectEnumerator] allObjects];
}

- (NSArray *)showingEvents
{
    NSArray *events = self.segmentControl.selectedSegmentIndex == 0 ? [self upcomingEvents] : [self pastEvents];
    return events;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Event *event = [self.tableModel objectAtIndexPath:indexPath];
    NSURL *imageURL = [NSURL URLWithString:event.image];
    TTImageViewController *imageVC = [[TTImageViewController alloc] initWithImageURL:imageURL];
    imageVC.hidesBottomBarWhenPushed = YES;
    imageVC.title = NSLocalizedString(@"Event", nil);
    [self.navigationController pushViewController:imageVC animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Event *event = [self.tableModel objectAtIndexPath:indexPath];
    return [TTEventCell heightForObject:event atIndexPath:indexPath tableView:tableView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _coursesLink = @"http://www.pa.gov.sg";
    
    NSArray *informationArr = [Information MR_findAll];
    NSLog(@"%@", informationArr);
    if([informationArr count] > 0) {

        _coursesLink = [[informationArr objectAtIndex:0] coursesLink];
    NSLog(@"%@", _coursesLink);
    }
    
    UIBarButtonItem *coursesButton = [[UIBarButtonItem alloc] initWithTitle:@"Courses" style:UIBarButtonItemStylePlain target:self action:@selector(showCourses:)];
    //self.navigationItem.rightBarButtonItem = coursesButton;
    
    self.tableModel = [[NITableViewModel alloc] initWithListArray:[self showingEvents] delegate:self];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void) showCourses:(id)sender
{
    NIWebController *webController = [[NIWebController alloc] initWithURL:[NSURL URLWithString:_coursesLink]];
    [self.navigationController pushViewController:webController animated:YES];
}

- (void)didSwitchEventMode:(id)sender
{
    self.tableModel = [[NITableViewModel alloc] initWithListArray:[self showingEvents] delegate:self];
    [self.tableView reloadData];
}

- (UITableViewCell *)tableViewModel: (NITableViewModel *)tableViewModel
                   cellForTableView: (UITableView *)tableView
                        atIndexPath: (NSIndexPath *)indexPath
                         withObject: (id)object
{
    UITableViewCell *cell;
    cell = [self.cellFactory tableViewModel:tableViewModel
                           cellForTableView:tableView
                                atIndexPath:indexPath
                                 withObject:object];
    if ([cell isKindOfClass:[TTEventCell class]]) {
        TTEventCell *eventCell = (TTEventCell *)cell;
        eventCell.callBlock = ^(TTEventCell *cell){
            [[UIApplication sharedApplication] callPhone:cell.event.phone];
        };
        eventCell.emailBlock = ^(TTEventCell *cell){
            MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
            [mailVC setToRecipients:@[cell.event.email]];
            mailVC.mailComposeDelegate = self;
            [self presentViewController:mailVC animated:YES completion:NULL];
        };
        eventCell.detailsBlock = ^(TTEventCell *cell) {
            Event *event = cell.event;
            NSURL *imageURL = [NSURL URLWithString:event.image];
            TTImageViewController *imageVC = [[TTImageViewController alloc] initWithImageURL:imageURL];
            imageVC.hidesBottomBarWhenPushed = YES;
            imageVC.title = NSLocalizedString(@"Event", nil);
            [self.navigationController pushViewController:imageVC animated:YES];
            
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        };
    }
    return cell;
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

@end
