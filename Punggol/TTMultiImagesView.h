//
//  TTMultiImageView.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TTMultiImagesViewDelegate;

@interface TTMultiImagesView : NIPagingScrollView

@property (nonatomic, strong) NSArray *images;
@property (nonatomic, assign) id<TTMultiImagesViewDelegate> delegate;

@end

@interface TTImageView : UIImageView <NIPagingScrollViewPage>

@end

@protocol TTMultiImagesViewDelegate <NSObject>

@required
- (void)multiImagesView:(TTMultiImagesView *)imagesView didTapImageView:(TTImageView *)imageView;

@end
