//
//  Facility.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FacilityImage;

@interface Facility : NSManagedObject

@property (nonatomic, retain) NSString * contactNumber;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * unitNo;
@property (nonatomic, retain) NSString * level;
@property (nonatomic, retain) NSSet *facilityImages;
@property (nonatomic, retain) NSNumber * id;

@end

@interface Facility (CoreDataGeneratedAccessors)

- (void)addFacilityImagesObject:(FacilityImage *)value;
- (void)removeFacilityImagesObject:(FacilityImage *)value;
- (void)addFacilityImages:(NSSet *)values;
- (void)removeFacilityImages:(NSSet *)values;

@end
