//
//  TTFloorPlanCell.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTFloorPlanCell.h"

@implementation TTFloorPlanObject

+ (id)floorPlanWithTitle:(NSString *)title imageURL:(NSString *)imageURL
{
    TTFloorPlanObject *object = [[TTFloorPlanObject alloc] init];
    object.title = title;
    object.imageURL = imageURL;
    return object;
}

@end

@implementation TTFloorPlanCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    TTFloorPlanObject *floorPlan = (TTFloorPlanObject *)object;
    if ([floorPlan isKindOfClass:[TTFloorPlanObject class]]) {
        self.textLabel.text = floorPlan.title;
    }
    return YES;
}

@end
