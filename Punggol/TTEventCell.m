//
//  TTEventCell.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/12/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "TTEventCell.h"
#import "Event.h"
#import "Event+Additions.h"
#import "Venue.h"

#define CELL_PADDING 5.0f
#define CONTENT_PADDING 10.0f
#define VIEW_PADDING 3.0f
#define TITLE_FONT [UIFont boldSystemFontOfSize:14.0f]
#define TIME_FONT [UIFont boldSystemFontOfSize:13.0f]
#define LOCATION_FONT TIME_FONT
#define DESC_FONT [UIFont systemFontOfSize:13.0f]
#define BUTTON_HEIGHT 36.0f
#define BUTTON_TOP_PADDING 20.0f

#define CELL_EDGE_INSETS UIEdgeInsetsMake(CELL_PADDING, CELL_PADDING, CELL_PADDING, CELL_PADDING)
#define CONTENT_EDGE_INSETS UIEdgeInsetsMake(CONTENT_PADDING, CONTENT_PADDING, CONTENT_PADDING, CONTENT_PADDING)

@interface TTEventCell () <MFMailComposeViewControllerDelegate>

@end

@implementation TTEventCell

+ (CGFloat)heightForObject:(Event *)event atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    CGFloat height = 0;
    
    CGFloat contentWidth = tableView.width - (CELL_PADDING + CONTENT_PADDING) * 2;
    CGSize contentSize = CGSizeMake(contentWidth, CGFLOAT_MAX);
    
    height += [event.title sizeWithFont:TITLE_FONT
                      constrainedToSize:contentSize
                          lineBreakMode:NSLineBreakByWordWrapping].height + VIEW_PADDING;
    
    height += [event.eventDuration sizeWithFont:TIME_FONT
                              constrainedToSize:contentSize
                                  lineBreakMode:NSLineBreakByWordWrapping].height + VIEW_PADDING;
    
    height += [event.venue.name sizeWithFont:LOCATION_FONT
                           constrainedToSize:contentSize
                               lineBreakMode:NSLineBreakByWordWrapping].height + VIEW_PADDING;
    
    height += BUTTON_HEIGHT + BUTTON_TOP_PADDING;
    
    height += (CELL_PADDING + CONTENT_PADDING)*3;
    
    return height;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.font = TITLE_FONT;
        [self.contentView addSubview:self.titleLabel];
        
        self.timeLabel = [[UILabel alloc] init];
        self.timeLabel.numberOfLines = 2;
        self.timeLabel.font = TIME_FONT;
        self.timeLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:self.timeLabel];
        
        self.locationLabel = [[UILabel alloc] init];
        self.locationLabel.numberOfLines = 1;
        self.locationLabel.font = LOCATION_FONT;
        self.locationLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:self.locationLabel];
        
        self.callButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.callButton setTitle:@"CALL" forState:UIControlStateNormal];
        [self applyStyleToButton:self.callButton];
        
        self.emailButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.emailButton setTitle:@"EMAIL" forState:UIControlStateNormal];
        [self applyStyleToButton:self.emailButton];
        
        self.detailsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.detailsButton setTitle:@"DETAILS" forState:UIControlStateNormal];
        [self applyStyleToButton:self.detailsButton];
  
        [self.contentView addSubview:self.callButton];
        [self.contentView addSubview:self.emailButton];
        [self.contentView addSubview:self.detailsButton];
        
        self.contentView.layer.borderWidth = 1.0f;
        self.contentView.layer.borderColor = [UIColor colorWithWhite:0.75f alpha:1.0f].CGColor;
        
        [self.callButton addTarget:self action:@selector(call) forControlEvents:UIControlEventTouchUpInside];
        [self.emailButton addTarget:self action:@selector(email) forControlEvents:UIControlEventTouchUpInside];
        [self.detailsButton addTarget:self action:@selector(details) forControlEvents:UIControlEventTouchUpInside];
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)applyStyleToButton:(UIButton *)button
{
    UIImage *backImage = [self imageWithColor: [UIColor colorWithRed:0.90f green:0.00f blue:0.00f alpha:1.00f]];
    [button setBackgroundImage:backImage forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:11.0f];
    button.layer.borderWidth = 1.0f;
    button.layer.borderColor = [UIColor colorWithRed:0.90f green:0.00f blue:0.00f alpha:0.80f].CGColor;
}

// http://stackoverflow.com/questions/990976/how-to-create-a-colored-1x1-uiimage-on-the-iphone-dynamically

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)call
{
    if (_callBlock) {
        _callBlock(self);
    }
}

- (void)email
{
    if (_emailBlock) {
        _emailBlock(self);
    }
}

- (void)details
{
    if (_detailsBlock) {
        _detailsBlock(self);
    }
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    Event *event = (Event *)object;
    if ([event isKindOfClass:[Event class]]) {
        self.titleLabel.text = event.title;
        self.timeLabel.text = event.eventDuration;
        self.locationLabel.text = event.venue.name;
        
        [self.imageView setImageWithURL: [NSURL URLWithString: event.image]
                       placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
        
        _event = event;
    } else {
        _event = nil;
    }
    return YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.contentView.frame = UIEdgeInsetsInsetRect(self.contentView.bounds, CELL_EDGE_INSETS);

    CGRect frame = UIEdgeInsetsInsetRect(self.contentView.bounds, CONTENT_EDGE_INSETS);
    
    CGSize sizeToFit;
    CGRect viewFrame;
    
    sizeToFit = [self.titleLabel sizeThatFits:CGSizeMake(frame.size.width - 90, CGFLOAT_MAX)];
    viewFrame = CGRectMake(frame.origin.x + 90, frame.origin.y, frame.size.width - 90, sizeToFit.height);
    self.titleLabel.frame = viewFrame;
    
    sizeToFit = [self.timeLabel sizeThatFits:CGSizeMake(frame.size.width - 90, CGFLOAT_MAX)];
    viewFrame = CGRectMake(frame.origin.x  + 90, CGRectGetMaxY(viewFrame) + VIEW_PADDING, frame.size.width - 90, sizeToFit.height);
    self.timeLabel.frame = viewFrame;
    
    sizeToFit = [self.locationLabel sizeThatFits:CGSizeMake(frame.size.width - 90, CGFLOAT_MAX)];
    viewFrame = CGRectMake(frame.origin.x  + 90, CGRectGetMaxY(viewFrame) + VIEW_PADDING, frame.size.width - 90, sizeToFit.height);
    self.locationLabel.frame = viewFrame;
    
    viewFrame = CGRectMake(frame.origin.x, frame.origin.y, 80, 80);
    self.imageView.frame = viewFrame;
    
    CGFloat buttonPadding = 0.0f;
    CGFloat buttonHeight = BUTTON_HEIGHT;
    CGFloat buttonWidth = floorf(self.contentView.width / 3);
    CGFloat buttonTop = floorf(self.contentView.height - buttonHeight);
    
    self.callButton.frame = CGRectMake(0, buttonTop, buttonWidth, buttonHeight);
    self.emailButton.frame = CGRectMake(self.callButton.width + buttonPadding, buttonTop, buttonWidth, buttonHeight);
    self.detailsButton.frame = CGRectMake(self.callButton.width + self.emailButton.width + buttonPadding, buttonTop, buttonWidth, buttonHeight);
}

- (void)dealloc
{
    self.callBlock = nil;
    self.emailBlock = nil;
    self.detailsBlock = nil;
}

@end
