//
//  TTTableViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NITableViewModel;

@interface TTTableViewController : UIViewController <UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NITableViewModel *tableModel;
@property (nonatomic, strong) NICellFactory *cellFactory;

- (UITableViewStyle)tableViewStyle;

@end
