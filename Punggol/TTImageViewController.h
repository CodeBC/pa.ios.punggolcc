//
//  TTImageViewController.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTImageViewController : UIViewController

@property (nonatomic, strong) NIPhotoScrollView *imageView;

- (id)initWithImageURL:(NSURL *)imageURL;
- (id)initWithImage:(UIImage *)image;

@end
