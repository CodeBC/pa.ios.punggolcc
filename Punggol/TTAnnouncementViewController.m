//
//  TTAnnouncementViewController.m
//  Punggol
//
//  Created by Ye Myat Min on 17/7/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTAnnouncementViewController.h"
#import "Feed.h"
#import "TTAnnouncementCell.h"

@interface TTAnnouncementViewController () 

@end

@implementation TTAnnouncementViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"News", nil);
        [self.cellFactory mapObjectClass:[Feed class] toCellClass:[TTAnnouncementCell class]];
        self.tabBarItem.image = [UIImage imageNamed:@"41-feed"];
    }
    return self;
}


- (void)loadView
{
    [super loadView];
    
    self.tableView.frame = CGRectMake(0, 0,
                                      self.view.width,
                                      self.view.height);
    self.tableView.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"cream_pixels.png"]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSArray *list = [Feed MR_findAll];
    self.tableModel = [[NITableViewModel alloc] initWithListArray:list delegate:self.cellFactory];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Feed *feed = [self.tableModel objectAtIndexPath:indexPath];
    return [TTAnnouncementCell heightForObject:feed atIndexPath:indexPath tableView:tableView];
}


@end
