//
//  UIApplication+PhoneCall.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "UIApplication+PhoneCall.h"
#import "NIInterapp.h"

@implementation UIApplication (PhoneCall)

static NSString *phoneNumberToCall;

- (void)callPhone:(NSString *)phoneNumber
{
    NSString *title = NSLocalizedString(@"Call", nil);
    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"Do you want to call %@ ?", nil), phoneNumber];
    NSString *cancel = NSLocalizedString(@"Cancel", nil);
    NSString *call = NSLocalizedString(@"Call", nil);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:cancel
                                          otherButtonTitles:call, nil];
    phoneNumberToCall = phoneNumber;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            
            break;
        case 1:
            [NIInterapp phoneWithNumber:phoneNumberToCall];
            break;
    }
}

@end
