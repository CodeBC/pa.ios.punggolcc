//
//  TTSocialMediaCell.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/16/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#define CELL_PADDING 5.0f
#define CONTENT_PADDING 10.0f
#define TITLE_FONT [UIFont boldSystemFontOfSize:16.0f]
#define CELL_EDGE_INSETS UIEdgeInsetsMake(CELL_PADDING, CELL_PADDING, CELL_PADDING, CELL_PADDING)
#define CONTENT_EDGE_INSETS UIEdgeInsetsMake(CONTENT_PADDING, CONTENT_PADDING, CONTENT_PADDING, CONTENT_PADDING)


#import "TTSocialMediaCell.h"
#import "SocialMedia.h"

@implementation TTSocialMediaCell

+ (CGFloat)heightForObject:(SocialMedia *)social atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    CGFloat height = 0;
    
    CGFloat contentWidth = tableView.width - (CELL_PADDING + CONTENT_PADDING) * 2;
    CGSize contentSize = CGSizeMake(contentWidth, CGFLOAT_MAX);
    
    height += [social.name sizeWithFont:TITLE_FONT
                     constrainedToSize:contentSize
                         lineBreakMode:NSLineBreakByWordWrapping].height + CELL_PADDING;

    
    height += (CELL_PADDING + CONTENT_PADDING)*2;
    
    return height;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        self.textLabel.numberOfLines = 2;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    SocialMedia *socialObject = (SocialMedia *)object;
    if ([socialObject isKindOfClass:[SocialMedia class]]) {
        self.textLabel.text = socialObject.name;
    }
    return YES;
}

@end