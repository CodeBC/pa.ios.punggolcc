//
//  TTEventCell.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/12/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTLabel.h"

@class TTEventCell;

typedef void (^action_block_t)(TTEventCell *cell);

@class Event;

@interface TTEventCell : UITableViewCell <NICell>

@property (nonatomic, strong) Event *event;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *locationLabel;
@property (nonatomic, strong) UIImageView *imageView;

@property (nonatomic, strong) UIButton *callButton;
@property (nonatomic, strong) UIButton *emailButton;
@property (nonatomic, strong) UIButton *detailsButton;

@property (nonatomic, copy) action_block_t callBlock;
@property (nonatomic, copy) action_block_t emailBlock;
@property (nonatomic, copy) action_block_t detailsBlock;

@end
