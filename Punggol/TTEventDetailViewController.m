//
//  TTEventDetailViewController.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/12/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTEventDetailViewController.h"
#import "TTImageViewController.h"
#import "UIApplication+PhoneCall.h"

@interface TTEventDetailViewController () <MFMailComposeViewControllerDelegate>

@end

@implementation TTEventDetailViewController

- (NSDictionary *)replaceValues
{
    if (self.event == nil) {
        return @{};
    }

    return @{
             @"title"   : self.event.title,
             @"desc"    : self.event.desc
             };
}

- (NSString *)viewTitle
{
    return @"Event";
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([request.URL.scheme isEqualToString:@"punggol"] && [request.URL.host isEqualToString:@"events"]) {
        if ([request.URL.path isEqualToString:@"/callPhone"]) {
            [[UIApplication sharedApplication] callPhone:self.event.phone];
            return NO;
        } else if ([request.URL.path isEqualToString:@"/sendEmail"]) {
            MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
            [mailVC setToRecipients:@[self.event.email]];
            mailVC.mailComposeDelegate = self;
            [self presentViewController:mailVC animated:YES completion:NULL];
            return NO;
        }
    }
    return YES;
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

@end
