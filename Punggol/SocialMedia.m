//
//  SocialMedia.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/18/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "SocialMedia.h"


@implementation SocialMedia

@dynamic name;
@dynamic value;
@dynamic sequence;
@dynamic id;

@end
