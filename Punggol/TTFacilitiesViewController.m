//
//  TTFacilitiesViewController.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/10/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTFacilitiesViewController.h"
#import "UISegmentedControl+TTAppearance.h"
#import "Facility.h"
#import "Facility+Additions.h"
#import "TTFacilityViewController.h"
#import "TTFacilityCell.h"

#import "TTFloorPlanCell.h"
#import "TTImageViewController.h"

@interface TTFacilitiesViewController () <UISearchDisplayDelegate, UITableViewDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) NITableViewModel *searchDataSouce;
@property (nonatomic, strong) UISegmentedControl *segmentControl;
@property (nonatomic, strong) UISearchDisplayController *retainSearchDisplayController; // fix the bug Apple ?

@property (nonatomic, strong) NICellFactory *cellFactory;

@property (nonatomic, strong) UITableView *floorPlanTableView;
@property (nonatomic, strong) UITableView *listTableView;

@property (nonatomic, strong) NITableViewModel *listTableViewDataSource;
@property (nonatomic, strong) NITableViewModel *floorPlanTableViewDataSource;

@property (nonatomic, strong) UIBarButtonItem * coursesButton;

@end

@implementation TTFacilitiesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Facilities", nil);
        self.tabBarItem.image = [UIImage imageNamed:@"103-map"];
        
        _cellFactory = [[NICellFactory alloc] init];
        [_cellFactory mapObjectClass:[TTFloorPlanObject class] toCellClass:[TTFloorPlanCell class]];
        [_cellFactory mapObjectClass:[Facility class] toCellClass:[TTFacilityCell class]];
    }
    return self;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.listTableView.sectionIndexColor = [UIColor grayColor];
    
    NSArray *facilities = [Facility MR_findAll];
    NSArray *grouped = [facilities groupedByKeyPath:@"title"];
    _listTableViewDataSource = [[NITableViewModel alloc] initWithSectionedArray:grouped delegate:_cellFactory];
    [_listTableViewDataSource setSectionIndexType:NITableViewModelSectionIndexAlphabetical
                                      showsSearch:NO
                                     showsSummary:YES];
    self.listTableView.dataSource = _listTableViewDataSource;
    [self.listTableView reloadData];
    
}

- (void)loadView
{
    [super loadView];
    
    // Search Bar
    
    UISearchBar *searchBar = [[UISearchBar alloc] init];
    [searchBar sizeToFit];
    
    _retainSearchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    self.searchDisplayController.delegate = self;
    _searchDataSouce = [[NITableViewModel alloc] init];
    
    self.searchDisplayController.searchResultsDataSource = _searchDataSouce;
    self.searchDisplayController.searchResultsDelegate = self;
    
    [self.view addSubview:searchBar];
    
    
    // Segmented Control
    
    UISegmentedControl *segmentControl = [[UISegmentedControl alloc] initWithItems:@[@"Floor Plan", @"List"]];
    segmentControl.segmentedControlStyle = UISegmentedControlStyleBar;
    [segmentControl sizeToFit];
    segmentControl.frame = CGRectMake(0, searchBar.height, searchBar.width, segmentControl.height);
    [segmentControl addTarget:self action:@selector(didSwitchMode:) forControlEvents:UIControlEventValueChanged];
    [segmentControl setSelectedSegmentIndex:1];
    [segmentControl setBackgroundColor:[UIColor whiteColor]];
    [segmentControl applyWhiteStyle];
//    [self.view addSubview:segmentControl];
    _segmentControl = segmentControl;
    
    //
    CGRect frame = CGRectMake(0, 0, self.view.width, self.view.height);
    self.floorPlanTableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    self.listTableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    
    self.floorPlanTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.listTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self.view addSubview:self.floorPlanTableView];
    [self.view addSubview:self.listTableView];
    
    NSArray *facilities = [Facility MR_findAll];
    NSArray *grouped = [facilities groupedByKeyPath:@"title"];
    _listTableViewDataSource = [[NITableViewModel alloc] initWithSectionedArray:grouped delegate:_cellFactory];
    [_listTableViewDataSource setSectionIndexType:NITableViewModelSectionIndexAlphabetical
                                      showsSearch:NO
                                     showsSummary:YES];
    self.listTableView.dataSource = _listTableViewDataSource;
    self.listTableView.delegate = self;
    
    NSArray *floorPlans = @[
                            [TTFloorPlanObject floorPlanWithTitle:@"Level 1" imageURL:@"https://s3.amazonaws.com/punggol/1.jpeg"],
                            [TTFloorPlanObject floorPlanWithTitle:@"Level 2" imageURL:@"https://s3.amazonaws.com/punggol/2.jpeg"],
                            [TTFloorPlanObject floorPlanWithTitle:@"Level 3" imageURL:@"https://s3.amazonaws.com/punggol/3.jpeg"],
                            [TTFloorPlanObject floorPlanWithTitle:@"Level 4" imageURL:@"https://s3.amazonaws.com/punggol/4.jpeg"]
                            ];
    _floorPlanTableViewDataSource = [[NITableViewModel alloc] initWithListArray:floorPlans delegate:_cellFactory];
    self.floorPlanTableView.dataSource = _floorPlanTableViewDataSource;
    self.floorPlanTableView.delegate = self;
    

}

- (void) viewDidLoad
{
    [super viewDidLoad];
    _coursesButton = [[UIBarButtonItem alloc] initWithTitle:@"Sort" style:UIBarButtonItemStylePlain target:self action:@selector(showSortActionSheet:)];
    self.navigationItem.rightBarButtonItem = _coursesButton;
    [self showList];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)showSortActionSheet:(id)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Sort By", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Level", nil), NSLocalizedString(@"Name", nil), nil];
    [sheet showFromBarButtonItem:sender animated:YES];
}


- (void)showFloorPlanList
{
    self.floorPlanTableView.hidden = NO;
    self.listTableView.hidden = YES;
}

- (void)showList
{
    self.floorPlanTableView.hidden = YES;
    self.listTableView.hidden = NO;
}

- (void)didSwitchMode:(id)sender
{
    switch (_segmentControl.selectedSegmentIndex) {
        case 0:
            [self showFloorPlanList];
            self.navigationItem.rightBarButtonItem = nil;
            break;
        case 1:
            [self showList];
            self.navigationItem.rightBarButtonItem = _coursesButton;
            break;
        default:
            break;
    }
    [self.floorPlanTableView reloadData];
    [self.listTableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _listTableView) {
        Facility *facility = [_listTableViewDataSource objectAtIndexPath:indexPath];
        TTFacilityViewController *facilityVC = [[TTFacilityViewController alloc] init];
        facilityVC.facility = facility;
        [self.navigationController pushViewController:facilityVC animated:YES];        
    } else if (tableView == _floorPlanTableView) {
        TTFloorPlanObject *floorPlan = [_floorPlanTableViewDataSource objectAtIndexPath:indexPath];
        NSURL *URL = [NSURL URLWithString:floorPlan.imageURL];
        TTImageViewController *imageVC = [[TTImageViewController alloc] initWithImageURL:URL];
        imageVC.title = floorPlan.title;
        [self.navigationController pushViewController:imageVC animated:YES];
    } else if (tableView == self.searchDisplayController.searchResultsTableView) {
        Facility *facility = [_searchDataSouce objectAtIndexPath:indexPath];
        TTFacilityViewController *facilityVC = [[TTFacilityViewController alloc] init];
        facilityVC.facility = facility;
        [self.navigationController pushViewController:facilityVC animated:YES];        
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64.0f;
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"title", searchString];
    NSArray *facilities = [Facility MR_findAllWithPredicate:predicate];
    
    _searchDataSouce = [[NITableViewModel alloc] initWithSectionedArray:[facilities groupedByKeyPath:@"title"]
                                                               delegate:_cellFactory];
    [_searchDataSouce setSectionIndexType:NITableViewModelSectionIndexAlphabetical
                              showsSearch:NO
                             showsSummary:YES];
    self.searchDisplayController.searchResultsDataSource = _searchDataSouce;
    
    return YES;
}

#pragma mark -
#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSArray *facilities = [self facilities];
    
    switch (buttonIndex) {
        case 0:
            _listTableViewDataSource = [[NITableViewModel alloc] initWithSectionedArray:[facilities groupFacilityByLevel]
                                                                     delegate:_cellFactory];
            [_listTableViewDataSource setSectionIndexType:NITableViewModelSectionIndexAlphabetical showsSearch:NO showsSummary:YES];
            self.listTableView.dataSource = _listTableViewDataSource;
            break;
        case 1:
            _listTableViewDataSource = [[NITableViewModel alloc] initWithSectionedArray:[facilities groupFacilityByName]
                                                                     delegate:_cellFactory];
            [_listTableViewDataSource setSectionIndexType:NITableViewModelSectionIndexAlphabetical showsSearch:NO showsSummary:YES];
            self.listTableView.dataSource = _listTableViewDataSource;
            break;
            
        default:
            break;
    }
     
    [self.listTableView reloadData];
}

- (NSArray *)facilities
{
    return [Facility MR_findAll];
}

@end
