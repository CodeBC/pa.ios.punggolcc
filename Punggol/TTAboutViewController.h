//
//  TTAboutViewController.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/10/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTAboutViewController : UIViewController

@property (nonatomic, strong) UITextView *textView;

@end
