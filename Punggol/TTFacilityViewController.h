//
//  TTFacilityViewController.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTHTMLTemplateViewController.h"
#import "Facility.h"

@interface TTFacilityViewController : TTHTMLTemplateViewController

@property (nonatomic, strong) Facility *facility;

@end
