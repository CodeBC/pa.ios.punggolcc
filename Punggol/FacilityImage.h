//
//  FacilityImage.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FacilityImage : NSManagedObject

@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * cropImage;
@property (nonatomic, retain) NSManagedObject *facility;

@end
