//
//  TTImageViewController.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTImageViewController.h"

@interface TTImageViewController ()

@property (nonatomic, strong) NSURL *imageToLoad;
@property (nonatomic, strong) UIImage *imageToShow;
@property (nonatomic, strong) id<SDWebImageOperation> imageDownloadOperation;

@end

@implementation TTImageViewController

- (id)initWithImageURL:(NSURL *)imageURL
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _imageToLoad = imageURL;
    }
    return self;
}

- (id)initWithImage:(UIImage *)image
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _imageToShow = image;
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    self.imageView = [[NIPhotoScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height)];
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.imageView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (_imageToLoad) {
        
        id<SDWebImageOperation> operation = [SDWebImageManager.sharedManager downloadWithURL:_imageToLoad
                                                                                     options:0
                                                                                    progress:NULL
                                                                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                                             {
                                                 [self.imageView setImage:image photoSize:NIPhotoScrollViewPhotoSizeOriginal];
                                             }];
        self.imageDownloadOperation = operation;
    }
    
    if (_imageToShow) {
        [self.imageView setImage:_imageToShow photoSize:NIPhotoScrollViewPhotoSizeOriginal];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
