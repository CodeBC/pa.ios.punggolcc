//
//  TTMapViewController.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/25/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTMapViewController : UIViewController

@property (nonatomic, strong) MKMapView *mapView;

@end
