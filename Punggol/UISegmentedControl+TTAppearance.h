//
//  UISegmentedControl+TTAppearance.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISegmentedControl (TTAppearance)

- (void)applyWhiteStyle;

@end
