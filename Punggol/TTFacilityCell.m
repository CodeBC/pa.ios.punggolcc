//
//  TTFacilityCell.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTFacilityCell.h"
#import "Facility.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

#define CELL_PADDING 5.0f
#define CONTENT_PADDING 10.0f
#define TITLE_FONT [UIFont boldSystemFontOfSize:16.0f]
#define CELL_EDGE_INSETS UIEdgeInsetsMake(CELL_PADDING, CELL_PADDING, CELL_PADDING, CELL_PADDING)
#define CONTENT_EDGE_INSETS UIEdgeInsetsMake(CONTENT_PADDING, CONTENT_PADDING, CONTENT_PADDING, CONTENT_PADDING)

@implementation TTFacilityCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    Facility *facility = (Facility *)object;
    if ([facility isKindOfClass:[Facility class]]) {
        self.textLabel.text = facility.title;
    
        
        if ([[facility.facilityImages allObjects] count] == 0) {
            [self.imageView setImageWithURL: [NSURL URLWithString:@"https://s3.amazonaws.com/punggol/Facilities-5-crop.JPG"]
                           usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        } else {
            [self.imageView setImageWithURL: [NSURL URLWithString:[[[facility.facilityImages allObjects] arrayOfKeyPath:@"cropImage"] objectAtIndex:0]]
                           usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
    }
    self.textLabel.numberOfLines = 2;
    return YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    
    CGRect viewFrame = CGRectMake(0, 0, 64, 64);
    self.imageView.frame = viewFrame;
    
    viewFrame = CGRectMake(74, 0, 200, 64);
    self.textLabel.frame = viewFrame;
    
}

+ (CGFloat)heightForObject:(Facility *)facility atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    CGFloat height = 0;
    
    CGFloat contentWidth = tableView.width - (CELL_PADDING + CONTENT_PADDING) * 2;
    CGSize contentSize = CGSizeMake(contentWidth, CGFLOAT_MAX);
    
    height += [facility.title sizeWithFont:TITLE_FONT
                      constrainedToSize:contentSize
                          lineBreakMode:NSLineBreakByWordWrapping].height + CELL_PADDING;
    
    height += (CELL_PADDING + CONTENT_PADDING)*2;
    
    return height;
}

@end
