//
//  TTAppDelegate.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/9/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTAppDelegate.h"

#import "TTAboutViewController.h"
#import "TTEventsViewController.h"
#import "TTFacilitiesViewController.h"
#import "TTSocialMediaViewController.h"
#import "TTAnnouncementViewController.h"

#import "Event.h"
#import "Facility.h"
#import "SocialMedia.h"
#import "Feed.h"
#import "Information.h"
#import "Utility.h"


#define IMPORT_DATA 1
#define IMPORT_FROM_LOCAL 0
#define DUMP_IMPORTED_DATA 0
#if IMPORT_DATA
    #define UPDATE_SEED_DB 0
#endif // IMPORT_DATA


#define PutInNavigationController(controller) [[UINavigationController alloc] initWithRootViewController:controller]

@implementation TTAppDelegate

+ (NSString *)SQLFilePath
{
    return [RKApplicationDataDirectory() stringByAppendingPathComponent:@"Data.sqlite"];
}

+ (RKManagedObjectStore *)defaultManagedObjectStore
{
    static RKManagedObjectStore *defaultManagedObjectStore;
    @synchronized(self) {
        if (!defaultManagedObjectStore) {
            NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
            defaultManagedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
            NSString *path = [self SQLFilePath];
#if IMPORT_DATA
            NSString *seedPath = nil;
            [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
#else
            NSString *seedPath = [[NSBundle mainBundle] pathForResource:@"Data" ofType:@"sqlite"];
#endif

            [defaultManagedObjectStore addSQLitePersistentStoreAtPath:path
                                               fromSeedDatabaseAtPath:seedPath
                                                    withConfiguration:nil
                                                              options:nil
                                                                error:nil];
            [defaultManagedObjectStore createManagedObjectContexts];
        }
    }
    return defaultManagedObjectStore;
}

- (void)uiSetup{
    if ([Utility isLessOSVersion:@"7.0"])
        [[UITabBar appearance] setSelectedImageTintColor: [UIColor colorWithRed:0.94f green:0.11f blue:0.11f alpha:1.00f]];
    else if([Utility isGreaterOREqualOSVersion:@"7.0"])
        [[UITabBar appearance] setTintColor: [UIColor colorWithRed:0.94f green:0.11f blue:0.11f alpha:1.00f]];
    
    if ([[UINavigationBar class]respondsToSelector:@selector(appearance)]) {
        [[UINavigationBar appearance]setTintColor:[UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.00f]]; // it set color of bar button item text
        [[UINavigationBar appearance]setBarTintColor:[UIColor colorWithRed:0.94f green:0.11f blue:0.11f alpha:1.00f]]; // it set color of navigation
    }
    
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        self.window.tintColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.00f];
    }
    self.window.backgroundColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.00f];
}

- (void)mapRestKitAndCoreData
{
    RKManagedObjectStore *managedObjectStore = [TTAppDelegate defaultManagedObjectStore];
    [NSManagedObjectContext MR_initializeDefaultContextWithCoordinator:managedObjectStore.persistentStoreCoordinator];

    //
    // Venue Mapping
    //
    RKEntityMapping *venueMapping = [RKEntityMapping mappingForEntityForName:@"Venue"
                                                        inManagedObjectStore:managedObjectStore];
    [venueMapping addAttributeMappingsFromDictionary:
     @{
     @"id": @"id",
     @"location": @"location",
     @"longitude" : @"longitude",
     @"latitude" : @"latitude",
     @"name" : @"name"
     }];

    // Shared Mapping Dictionary

    NSDictionary *mapping = @{
                              @"address": @"address",
                              @"email": @"email",
                              @"name" : @"name",
                              @"phone" : @"phone",
                              @"title" : @"title",
                              @"description" : @"desc"
                              };

    //
    // Organizer Mapping
    //
    RKEntityMapping *organizerMapping = [RKEntityMapping mappingForEntityForName:@"Organizer"
                                                            inManagedObjectStore:managedObjectStore];
    [organizerMapping addAttributeMappingsFromDictionary:mapping];

    //
    // Contact Mapping
    //
    RKEntityMapping *contactMapping = [RKEntityMapping mappingForEntityForName:@"Contact"
                                                          inManagedObjectStore:managedObjectStore];
    [contactMapping addAttributeMappingsFromDictionary:mapping];

    //
    // Guest Mapping
    //
    RKEntityMapping *guestMapping = [RKEntityMapping mappingForEntityForName:@"Guest"
                                                        inManagedObjectStore:managedObjectStore];
    [guestMapping addAttributeMappingsFromDictionary:mapping];

    //
    // Event Mapping
    //
    RKEntityMapping *eventMapping = [RKEntityMapping mappingForEntityForName:@"Event"
                                                        inManagedObjectStore:managedObjectStore];
    [eventMapping addAttributeMappingsFromDictionary:
     @{
     @"id": @"id",
     @"title": @"title",
     @"end_datetime" : @"endDateTime",
     @"start_datetime" : @"startDateTime",
     @"image" : @"image",
     @"crop_image" : @"cropImage",
     @"email": @"email",
     @"phone" : @"phone",
     @"description" : @"desc"
     }];

    [eventMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"venue"
                                                                                 toKeyPath:@"venue"
                                                                               withMapping:venueMapping]];
    [eventMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"organizers"
                                                                                 toKeyPath:@"organizers"
                                                                               withMapping:organizerMapping]];
    [eventMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"guest_of_honours"
                                                                                 toKeyPath:@"guestOfHonours"
                                                                               withMapping:guestMapping]];
    [eventMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"contacts"
                                                                                 toKeyPath:@"contacts"
                                                                               withMapping:contactMapping]];

    //
    // Announcement Mapping
    //
    RKEntityMapping *announcementMapping = [RKEntityMapping mappingForEntityForName:@"Feed"
                                                        inManagedObjectStore:managedObjectStore];
    [announcementMapping addAttributeMappingsFromDictionary:
     @{
     @"id": @"id",
     @"title": @"title",
     @"description" : @"desc",
     @"created_at" : @"deliveredDate"
     }];

    //
    // Information Mapping
    //
    RKEntityMapping *informationMapping = [RKEntityMapping mappingForEntityForName:@"Information"
                                                               inManagedObjectStore:managedObjectStore];
    [informationMapping addAttributeMappingsFromDictionary:
     @{
     @"id": @"id",
     @"address": @"address",
     @"course_link" : @"coursesLink"
     }];



    //
    // FacilityImage Mapping
    //
    RKEntityMapping *facilityImageMapping = [RKEntityMapping mappingForEntityForName:@"FacilityImage"
                                                                inManagedObjectStore:managedObjectStore];
    [facilityImageMapping addAttributeMappingsFromDictionary:
     @{
     @"image": @"image",
     @"crop_image": @"cropImage"
     }];

    //
    // Facility Mapping
    //
    RKEntityMapping *facilityMapping = [RKEntityMapping mappingForEntityForName:@"Facility"
                                                           inManagedObjectStore:managedObjectStore];
    [facilityMapping addAttributeMappingsFromDictionary:
     @{
     @"contact_number": @"contactNumber",
     @"title" : @"title",
     @"unit_no" : @"unitNo",
     @"level" : @"level",
     @"email": @"email",
     @"description" : @"desc",
     @"id" : @"id"
     }];

    [facilityMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"facility_images"
                                                                                    toKeyPath:@"facilityImages"
                                                                                  withMapping:facilityImageMapping]];

    //
    // Social Mapping
    //
    RKEntityMapping *socialMediaMapping = [RKEntityMapping mappingForEntityForName:@"SocialMedia"
                                                              inManagedObjectStore:managedObjectStore];
    [socialMediaMapping addAttributeMappingsFromDictionary:
     @{
     @"name": @"name",
     @"value" : @"value",
     @"sequence" : @"sequence",
     @"id" : @"id"
     }];


    //1978-02-06T22:20:04Z
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [RKObjectMapping setPreferredDateFormatter:formatter];
    [RKObjectMapping addDefaultDateFormatter:formatter];

#if IMPORT_DATA

    NSManagedObjectModel *model = managedObjectStore.managedObjectModel;
    for (NSEntityDescription *entity in model.entities) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity = entity;
        NSManagedObjectContext *context = [managedObjectStore persistentStoreManagedObjectContext];
        [context performBlockAndWait:^{
            NSError *error;
            NSArray *managedObjects = [context executeFetchRequest:fetchRequest error:&error];
            for (NSManagedObject *managedObject in managedObjects) {
                [context deleteObject:managedObject];
            }
        }];
    }
#endif // IMPORT_DATA
    NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful); // Anything in 2xx

    NSOperationQueue *queue = [[NSOperationQueue alloc] init];

#pragma mark - Import Event from API
    //
    // Import Event from API
    //
    {
        [Event MR_truncateAll];
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:eventMapping
                                                                                           pathPattern:nil
                                                                                               keyPath:@""
                                                                                           statusCodes:statusCodes];
#if IMPORT_FROM_LOCAL
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/punggol/events.json"]];
#else
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://punggolcc.herokuapp.com/api/v1/events"]];
#endif

        RKManagedObjectRequestOperation *operation = [[RKManagedObjectRequestOperation alloc] initWithRequest:request
                                                                                          responseDescriptors:@[responseDescriptor]];
        operation.managedObjectContext = managedObjectStore.mainQueueManagedObjectContext;
        operation.managedObjectCache = managedObjectStore.managedObjectCache;
        [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            __block BOOL success;
            __block NSError *localError = nil;
            [managedObjectStore.mainQueueManagedObjectContext performBlockAndWait:^{
                success = [managedObjectStore.mainQueueManagedObjectContext save:&localError];
                if (!success) {
                    RKLogCoreDataError(localError);
                }
            }];

#if DUMP_IMPORTED_DATA
            NSArray *events = [Event MR_findAll];
            NSAssert(events.count, @"No Events Found");
            for (Event *event in events) {
                NSLog(@"%@", @"=== ID ===");
                NSLog(@"%@", event.id);
                NSLog(@"%@", @"=== Title ===");
                NSLog(@"%@", event.title);
                NSLog(@"%@", event.startDateTime);
                NSLog(@"%@", @"=== Location ===");
                NSLog(@"%@", event.venue);
                NSLog(@"%@", @"=== ==== ===");
            }
#endif

        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NIDPRINT(@"Error importing events: %@", error);
        }];

        [queue addOperation:operation];
    }

#pragma mark - Import Factility from API
    //
    // Import Factility from API
    //
    {
        [Facility MR_truncateAll];
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:facilityMapping
                                                                                           pathPattern:nil
                                                                                               keyPath:@""
                                                                                           statusCodes:statusCodes];
#if IMPORT_FROM_LOCAL
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/punggol/facilities.json"]];
#else
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://punggolcc.herokuapp.com/api/v1/facilities"]];
#endif

        RKManagedObjectRequestOperation *operation = [[RKManagedObjectRequestOperation alloc] initWithRequest:request
                                                                                          responseDescriptors:@[responseDescriptor]];
        operation.managedObjectContext = managedObjectStore.mainQueueManagedObjectContext;
        operation.managedObjectCache = managedObjectStore.managedObjectCache;
        [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            __block BOOL success;
            __block NSError *localError = nil;
            [managedObjectStore.mainQueueManagedObjectContext performBlockAndWait:^{
                success = [managedObjectStore.mainQueueManagedObjectContext save:&localError];
                if (!success) {
                    RKLogCoreDataError(localError);
                }
            }];

#if DUMP_IMPORTED_DATA
            NSArray *item = [Facility MR_findAll];
            NSAssert(item.count, @"No Facility Found");
            for (Facility *facility in item) {
                NSLog(@"%@", @"=== Title ===");
                NSLog(@"%@", facility.title);

                NSLog(@"%@", @"=== Email ===");
                NSLog(@"%@", facility.email);

                NSLog(@"%@", @"=== ==== ===");
            }
#endif

        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NIDPRINT(@"Error importing facility: %@", error);
        }];

        [queue addOperation:operation];
    }

#pragma mark - Import SocialMedia from API
    //
    // Import SocialMedia from API
    //
    {
        [SocialMedia MR_truncateAll];
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:socialMediaMapping
                                                                                           pathPattern:nil
                                                                                               keyPath:@""
                                                                                           statusCodes:statusCodes];
#if IMPORT_FROM_LOCAL
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/punggol/settings.json"]];
#else
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://punggolcc.herokuapp.com/api/v1/settings"]];
#endif // IMPORT_FROM_LOCAL

        RKManagedObjectRequestOperation *operation = [[RKManagedObjectRequestOperation alloc] initWithRequest:request
                                                                                          responseDescriptors:@[responseDescriptor]];
        operation.managedObjectContext = managedObjectStore.mainQueueManagedObjectContext;
        operation.managedObjectCache = managedObjectStore.managedObjectCache;
        [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            __block BOOL success;
            __block NSError *localError = nil;
            [managedObjectStore.mainQueueManagedObjectContext performBlockAndWait:^{
                success = [managedObjectStore.mainQueueManagedObjectContext save:&localError];
                if (!success) {
                    RKLogCoreDataError(localError);
                }
            }];

#if DUMP_IMPORTED_DATA
            NSArray *item = [SocialMedia MR_findAll];
            NSAssert(item.count, @"No SocialMedia Found");
            for (SocialMedia *socialMedia in item) {
                NSLog(@"%@", @"=== Title ===");
                NSLog(@"%@", socialMedia.name);

                NSLog(@"%@", @"=== Value ===");
                NSLog(@"%@", socialMedia.value);

                NSLog(@"%@", @"=== ==== ===");
            }
#endif // DUMP_IMPORTED_DATA

        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NIDPRINT(@"Error importing SocialMedia: %@", error);
        }];

        [queue addOperation:operation];
    }

#pragma mark - Import Announcement from API
    //
    // Import SocialMedia from API
    //
    {
        [Feed MR_truncateAll];
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:announcementMapping
                                                                                           pathPattern:nil
                                                                                               keyPath:@""
                                                                                           statusCodes:statusCodes];
#if IMPORT_FROM_LOCAL
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/punggol/announcement.json"]];
#else
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://punggolcc.herokuapp.com/api/v1/announcements"]];
#endif // IMPORT_FROM_LOCAL

        RKManagedObjectRequestOperation *operation = [[RKManagedObjectRequestOperation alloc] initWithRequest:request
                                                                                          responseDescriptors:@[responseDescriptor]];
        operation.managedObjectContext = managedObjectStore.mainQueueManagedObjectContext;
        operation.managedObjectCache = managedObjectStore.managedObjectCache;
        [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            __block BOOL success;
            __block NSError *localError = nil;
            [managedObjectStore.mainQueueManagedObjectContext performBlockAndWait:^{
                success = [managedObjectStore.mainQueueManagedObjectContext save:&localError];
                if (!success) {
                    RKLogCoreDataError(localError);
                }
            }];
#if DUMP_IMPORTED_DATA
            NSArray *item = [Feed MR_findAll];
            NSAssert(item.count, @"No Announcement Found");
            for (Feed *announcement in item) {
                NSLog(@"%@", @"=== Title ===");
                NSLog(@"%@", socialMedia.title);

                NSLog(@"%@", @"=== Desc ===");
                NSLog(@"%@", socialMedia.desc);

                NSLog(@"%@", @"=== ==== ===");
            }
#endif // DUMP_IMPORTED_DATA

        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NIDPRINT(@"Error importing Announcement: %@", error);
        }];

        [queue addOperation:operation];
    }

#pragma mark - Import Information from API
    //
    // Import Information from API
    //
    {
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:informationMapping
                                                                                           pathPattern:nil
                                                                                               keyPath:@""
                                                                                           statusCodes:statusCodes];
#if IMPORT_FROM_LOCAL
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/punggol/information.json"]];
#else
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://punggolcc.herokuapp.com/api/v1/information"]];
#endif // IMPORT_FROM_LOCAL

        RKManagedObjectRequestOperation *operation = [[RKManagedObjectRequestOperation alloc] initWithRequest:request
                                                                                          responseDescriptors:@[responseDescriptor]];
        operation.managedObjectContext = managedObjectStore.mainQueueManagedObjectContext;
        operation.managedObjectCache = managedObjectStore.managedObjectCache;
        [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            __block BOOL success;
            __block NSError *localError = nil;
            [managedObjectStore.mainQueueManagedObjectContext performBlockAndWait:^{
                success = [managedObjectStore.mainQueueManagedObjectContext save:&localError];
                if (!success) {
                    RKLogCoreDataError(localError);
                }
            }];

#if DUMP_IMPORTED_DATA
            NSArray *item = [Feed MR_findAll];
            NSAssert(item.count, @"No Information Found");
            for (Information *info in item) {
                NSLog(@"%@", @"=== Course Link ===");
                NSLog(@"%@", info.coursesLink);

                NSLog(@"%@", @"=== ==== ===");
            }
#endif // DUMP_IMPORTED_DATA

        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NIDPRINT(@"Error importing Announcement: %@", error);
        }];

        [queue addOperation:operation];
    }


#if UPDATE_SEED_DB
    dispatch_async(dispatch_queue_create(NULL, DISPATCH_QUEUE_CONCURRENT), ^{
        NSError *error;
        [queue waitUntilAllOperationsAreFinished];
        NSString *seedSQL = [NSString stringWithFormat:@"%s/Data.sqlite", dirname(__FILE__)];
        NSFileManager *manager = [NSFileManager defaultManager];
        if ([manager fileExistsAtPath:seedSQL]) {
            [manager removeItemAtPath:seedSQL error:&error];
        }
        [manager copyItemAtPath:[TTAppDelegate SQLFilePath] toPath:seedSQL error:&error];
        if (error == nil) {
            NSLog(@"SeedDB updated");
        } else {
            NSLog(@"Error updating SeedDB: %@", error);
        }
    });
#endif // UPDATE_SEED_DB
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self mapRestKitAndCoreData];

    NSLog(@"Register notifications");
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeBadge)];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;


//    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:187.0/255.0 green:23.0/255.0 blue:0 alpha:1]];
//    [[UITabBarItem appearance] setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor]}
//                                             forState:UIControlStateNormal];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    UIViewController *viewController1 = [[TTAboutViewController alloc] init];
    UIViewController *viewController2 = [[TTEventsViewController alloc] init];
    UIViewController *viewController3 = [[TTFacilitiesViewController alloc] init];
    UIViewController *viewController4 = [[TTSocialMediaViewController alloc] init];
    UIViewController *viewController5 = [[TTAnnouncementViewController alloc] init];
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.viewControllers = @[PutInNavigationController(viewController1),
                                              PutInNavigationController(viewController2),
                                              PutInNavigationController(viewController3),
                                              PutInNavigationController(viewController4),
                                              PutInNavigationController(viewController5)];
    self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];

    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:0.90f green:0.00f blue:0.00f alpha:1.00f]];
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:0.90f green:0.00f blue:0.00f alpha:1.00f]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                UITextAttributeTextColor: [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0],
                          UITextAttributeTextShadowColor: [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8],
                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
                                     UITextAttributeFont: [UIFont fontWithName:@"AvenirNext-Regular" size:21.0]
     }];
    
    [self uiSetup];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Show the device token obtained from apple to the log
    NSString *deviceTokenStr = [[[[deviceToken description]
                                  stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                 stringByReplacingOccurrencesOfString: @">" withString: @""]
                                stringByReplacingOccurrencesOfString: @" " withString: @""];

    NSLog(@"Submitting Device Token: %@", deviceTokenStr);

    NSURL *url = [NSURL URLWithString:@"http://punggolcc.herokuapp.com/api/v1"];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];

    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            deviceTokenStr, @"device[token]",
                            @"ios", @"device[device_type]",
                            @"0", @"device[badge]",
                            nil];
    [httpClient postPath:@"/api/v1/devices" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"Request Successful, response '%@'", responseStr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];
}

- (void)application:(UIApplication *)application  didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [self.tabBarController setSelectedIndex:4];
}

@end
