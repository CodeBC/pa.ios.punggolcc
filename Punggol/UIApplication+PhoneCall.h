//
//  UIApplication+PhoneCall.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (PhoneCall)

- (void)callPhone:(NSString *)phoneNumber;

@end
