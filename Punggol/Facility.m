//
//  Facility.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Facility.h"
#import "FacilityImage.h"


@implementation Facility

@dynamic contactNumber;
@dynamic desc;
@dynamic email;
@dynamic title;
@dynamic unitNo;
@dynamic level;
@dynamic facilityImages;
@dynamic id;

@end
