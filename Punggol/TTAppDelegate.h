//
//  TTAppDelegate.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/9/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tabBarController;

@end
