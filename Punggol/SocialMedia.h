//
//  SocialMedia.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/18/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SocialMedia : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) NSNumber * sequence;
@property (nonatomic, retain) NSNumber * id;

@end
