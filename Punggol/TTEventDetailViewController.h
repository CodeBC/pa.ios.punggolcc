//
//  TTEventDetailViewController.h
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/12/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTHTMLTemplateViewController.h"
#import "Event.h"

@interface TTEventDetailViewController : TTHTMLTemplateViewController

@property (nonatomic, strong) Event *event;

@end
