//
//  TTSocialMediaViewController.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/10/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTSocialMediaViewController.h"
#import "TTSocialMediaCell.h"
#import "SocialMedia.h"

@interface TTSocialMediaViewController ()

@property (nonatomic, strong) UIImageView *bannerImageView;

@end

@implementation TTSocialMediaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Facebook", nil);
        [self.cellFactory mapObjectClass:[SocialMedia class] toCellClass:[TTSocialMediaCell class]];
        self.tabBarItem.image = [UIImage imageNamed:@"286-speechbubble"];
    }
    return self;
}

- (UITableViewStyle)tableViewStyle
{
    return UITableViewStyleGrouped;
}

- (void)loadView
{
    [super loadView];
        
    self.bannerImageView = [[UIImageView alloc] init];
    self.bannerImageView.frame = CGRectMake(0, 0, self.view.width, 125.0f);

    [self.bannerImageView setImageWithURL:[NSURL URLWithString:@"https://s3.amazonaws.com/punggol/facebook-banner.jpg"]
                        placeholderImage:nil];
    self.bannerImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view insertSubview:self.bannerImageView belowSubview:self.tableView];
    
    self.tableView.frame = CGRectMake(0, self.bannerImageView.height,
                                      self.view.width,
                                      self.view.height - self.bannerImageView.height);
}

- (void) viewDidLoad {
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSArray *list = [SocialMedia MR_findAll];
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"sequence" ascending:YES];
    NSArray *sorted = [list sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
    self.tableModel = [[NITableViewModel alloc] initWithListArray:sorted delegate:self.cellFactory];
    [self.tableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SocialMedia *object = [self.tableModel objectAtIndexPath:indexPath];
    NIWebController *webController = [[NIWebController alloc] initWithURL:[NSURL URLWithString:object.value]];
    [self.navigationController pushViewController:webController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SocialMedia *social = [self.tableModel objectAtIndexPath:indexPath];
    return [TTSocialMediaCell heightForObject:social atIndexPath:indexPath tableView:tableView];
}

@end
