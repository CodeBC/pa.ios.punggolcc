//
//  TTMultiImageView.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTMultiImagesView.h"

@interface TTMultiImagesView () <NIPagingScrollViewDataSource, NIPagingScrollViewDelegate>

@property (nonatomic, strong) NIPagingScrollView *scrollView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

@end

@implementation TTMultiImagesView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.scrollView = [[NIPagingScrollView alloc] init];
        self.scrollView.dataSource = self;
        self.scrollView.delegate = self;
        self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.scrollView.frame = CGRectMake(0, 0, self.width, self.height);
        
        self.pageControl = [[UIPageControl alloc] init];
        self.pageControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.pageControl.frame = CGRectMake(0, self.height - 30.0f, self.width, 30.0f);
        [self.pageControl addTarget:self action:@selector(currentPageDidChange:) forControlEvents:UIControlEventValueChanged];
        
        [self addSubview:self.scrollView];
        [self addSubview:self.pageControl];
        
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
        [self.scrollView addGestureRecognizer:_tapGesture];
    }
    return self;
}

- (void)setImages:(NSArray *)images
{
    if (images == _images) {
        return;
    }
    _images = images;
    [self.scrollView reloadData];
}

- (void)currentPageDidChange:(id)sender
{
    [self.scrollView moveToPageAtIndex:self.pageControl.currentPage animated:YES];
}

#pragma mark - NIPagingScrollViewDataSource

- (NSInteger)numberOfPagesInPagingScrollView:(NIPagingScrollView *)pagingScrollView
{
    self.pageControl.numberOfPages = self.images.count;
    return self.images.count;
}

- (UIView<NIPagingScrollViewPage> *)pagingScrollView:(NIPagingScrollView *)pagingScrollView pageViewForIndex:(NSInteger)pageIndex
{
    NSString* reuseIdentifier = @"photo";
    TTImageView *imageView;
    imageView = (TTImageView *)[pagingScrollView dequeueReusablePageWithIdentifier:reuseIdentifier];
    if (imageView == nil) {
        imageView = [[TTImageView alloc] init];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        imageView.reuseIdentifier = reuseIdentifier;
        imageView.userInteractionEnabled = YES;
    }
    [imageView setImageWithURL:[NSURL URLWithString:self.images[pageIndex]]];
    return imageView;
}

#pragma mark - NIPagingScrollViewDelegate

- (void)pagingScrollViewDidChangePages:(NIPagingScrollView *)pagingScrollView
{
    self.pageControl.currentPage = pagingScrollView.centerPageIndex;
}

#pragma mark - 

- (void)didTap:(id)sender
{
    if (self.delegate) {
        [self.delegate multiImagesView:self didTapImageView:(TTImageView *)self.scrollView.centerPageView];
    }
}

@end

@implementation TTImageView

@synthesize pageIndex;
@synthesize reuseIdentifier;

- (void)prepareForReuse
{
    self.image = nil;
}

@end
