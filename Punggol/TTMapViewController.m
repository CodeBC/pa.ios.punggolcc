//
//  TTMapViewController.m
//  Punggol
//
//  Created by Thant Thet Khin Zaw on 6/25/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTMapViewController.h"

@interface TTMapViewController ()

@end

@implementation TTMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height)];
    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.mapView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(1.374143, 103.891883);
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(centerCoordinate, 500, 500);
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:centerCoordinate];
    [annotation setTitle:@"Punggol Community Club"]; //You can set the subtitle too
    [self.mapView addAnnotation:annotation];
    [self.mapView setRegion:region];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
